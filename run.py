#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["E. Ulises Moya", "Sabastia Xambo", "Abraham Sanchez", "Raul Nanclares", "Jorge Martinez", "Alexander Quevedo", "Baowei Fei"]
__copyright__ = "Copyright 2020, Gobierno de Jalisco, Universitat Politecnica de Catalunya, University of Texas at Dallas"
__credits__ = ["E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["E. Ulises Moya", "Abraham Sanchez"]
__email__ = "eduardo.moya@jalisco.gob.mx"
__status__ = "Development"


import os
import argparse

from executor_test import TestExecutor
from executor_training import TrainExecutor

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--result', '-r', required=True, help='Results directory')
    parser.add_argument('--runtype', '-t', default='train', type=str, help='Execution type [train|eval]')
    parser.add_argument('--epochs', '-e', default=100, type=int, help='Epochs used in train process')
    parser.add_argument('--lr', '-l', default=0.001, type=float, help='Learning rate')
    parser.add_argument('--batch', '-b', default=128, type=int, help='Batch size')
    args = parser.parse_args()
    data_config = {
        # Dataset name | Classes | Image size | Split size
        'mnist': ('mnist', 10, (28, 28, 1), ['train[:80%]', 'train[80%:]', 'test']),
        'fashion_mnist': ('fashion_mnist', 10, (28, 28, 1), ['train[:80%]', 'train[80%:]', 'test']),
        'cifar10': ('cifar10', 10, (32, 32, 3), ['train[:80%]', 'train[80%:]', 'test']),
        'cats_vs_dogs': ('cats_vs_dogs', 2, (128, 128, 3), ['train[:70%]', 'train[70%:85%]', 'train[85%:]'])
    }

    if args.runtype == 'train':
        trainer = TrainExecutor(data_config=data_config, output_folder_name=args.result, epochs=args.epochs, lr=args.lr,
                                batch_size=args.batch)
        trainer.execute()
    elif args.runtype == 'eval':
        tester = TestExecutor(data_config=data_config, output_folder_name=args.result, lr=args.lr,
                              batch_size=args.batch)
        tester.execute()

    print('Done!')
