#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["E. Ulises Moya", "Sabastia Xambo", "Abraham Sanchez", "Raul Nanclares", "Jorge Martinez", "Alexander Quevedo", "Baowei Fei"]
__copyright__ = "Copyright 2020, Gobierno de Jalisco, Universitat Politecnica de Catalunya, University of Texas at Dallas"
__credits__ = ["E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["E. Ulises Moya", "Abraham Sanchez"]
__email__ = "eduardo.moya@jalisco.gob.mx"
__status__ = "Development"


import numpy as np
import tensorflow as tf

from tensorflow.keras.callbacks import Callback

from layers.m6 import M6


class M6CallBack(Callback):
    
    def __init__(self, model):
        super(M6CallBack).__init__()
        self.weights = list()
        self.layer = None
        try:
            self.layer = model.get_layer('m6')
            s, mw, mlt, sigma = self.layer.get_weights()
            self.weights.append([0, s, mw, mlt, sigma])
        except:
            print("[Callback] - M6 layer not found. Discarding callback's actions for this model.")

    def on_epoch_end(self, epoch, logs=None):
        if self.layer is not None:
            s, mw, mlt, sigma = self.layer.get_weights()
            self.weights.append([epoch+1, s, mw, mlt, sigma])

    def get_weights(self):
        return np.array(self.weights)

